﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Point : MonoBehaviour
{
    private Transform _transform;
    private MeshRenderer _renderer;
    private float _currentAlpha = 0;
    void Start()
    {
        _transform = transform;
        _renderer = GetComponent<MeshRenderer>();
    }

    void Update()
    {
        if (_currentAlpha > 0)
        {
            SetColor(_currentAlpha);
            _currentAlpha = Mathf.Max(0, _currentAlpha - Time.deltaTime * 0.2f);
        }
    }

    public void SetPosition(Vector3 pos)
    {
        Debug.Log(pos);
        pos.y = _transform.position.y;
        _transform.position = pos;
        _currentAlpha = 1;
    }

    private void SetColor(float alpha)
    {   
        _renderer.material.color = new Color(1, 0, 0, alpha);
    }
}
