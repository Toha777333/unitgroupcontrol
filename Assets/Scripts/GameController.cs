﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    private static GameController _instance;
    public static GameController Instance => _instance;

    public Action ClickLBMUp;
    public Action ClickLBMDown;
    public Action<Vector3> ClickRBM;

    public Camera MainCamera;
    public Point TargetPoint;

    public GUISkin Skin;
    private Rect _rect;
    private bool _draw;
    private Vector2 _startPos;
    private Vector2 _endPos;

    void Awake()
    {
        if (_instance == null)
        { 
            _instance = this; 
        }
        else if (_instance == this)
        { 
            Destroy(gameObject);
        }
    }

    void Select()
    {
        ClickLBMUp.Invoke();
    }

    public bool CheckSelect(Transform target)
    {
        Vector2 tmp = new Vector2(
            Camera.main.WorldToScreenPoint(target.position).x, 
            Screen.height - Camera.main.WorldToScreenPoint(target.position).y);

        return _rect.Contains(tmp);
    }

    void Deselect()
    {
        ClickLBMDown.Invoke();
    }

    void OnGUI()
    {
        GUI.skin = Skin;
        GUI.depth = 99;

        if (Input.GetMouseButtonDown(0))
        {
            Deselect();
            _startPos = Input.mousePosition;
            _draw = true;
        }

        if (Input.GetMouseButtonUp(0) && _draw)
        {
            _draw = false;
            Select();
        }

        if (_draw)
        {
            DrawRect();
        }

        if (Input.GetMouseButtonDown(1))
        {
            SetPKMClick();
        }
    }

    private void DrawRect()
    {
        _endPos = Input.mousePosition;
        if (_startPos == _endPos) return;

        _rect = new Rect(Mathf.Min(_endPos.x, _startPos.x),
                        Screen.height - Mathf.Max(_endPos.y, _startPos.y),
                        Mathf.Max(_endPos.x, _startPos.x) - Mathf.Min(_endPos.x, _startPos.x),
                        Mathf.Max(_endPos.y, _startPos.y) - Mathf.Min(_endPos.y, _startPos.y)
                        );

        GUI.Box(_rect, "");
    }

    private void SetPKMClick()
    {
        Ray ray = MainCamera.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(ray.origin, ray.direction, out hit))
        {
            TargetPoint.SetPosition(hit.point);
            ClickRBM.Invoke(hit.point);
        }
    }
}
