﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class UnitControl : MonoBehaviour
{
    private bool _isSelect;
    private bool _isMove = false;
    private Transform _transform;
    private MeshRenderer _renderer;

    private Vector3 _targetPosition;

    void Start()
    {
        _transform = transform;
        _renderer = GetComponent<MeshRenderer>();
        GameController.Instance.ClickLBMUp += Select;
        GameController.Instance.ClickLBMDown += Deselect;
        GameController.Instance.ClickRBM += SetPosition;

        _targetPosition = _transform.position;
    }

    private void Update()
    {
        if(_isMove)
            _transform.position = Vector3.MoveTowards(_transform.position, _targetPosition, Time.deltaTime * 2);
    }

    private void Select()
    {
        if (!GameController.Instance.CheckSelect(_transform)) return;
        _renderer.material.color = Color.red;
        _isSelect = true;
    }

    private void Deselect()
    {
        if (!_isSelect) return;

        _isSelect = false;
        _renderer.material.color = Color.white;
    }

    public void SetPosition(Vector3 pos2)
    {
        if (!_isSelect) return;

        pos2.y = _targetPosition.y;
        _targetPosition = pos2;

        _isMove = true;
    }
}
